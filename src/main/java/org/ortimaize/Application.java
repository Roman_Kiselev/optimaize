package org.ortimaize;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Application {
    public static void main(String[] args) throws IOException {
        System.out.println("Write a number. to get the average value enter avg, the maximum number - max, " +
                "the minimum number - min, the count - count, all parameters - all, for exit - exit");
        BufferedReader reader =
                new BufferedReader(new InputStreamReader(System.in));
        NumbersKeeper numbersKeeper = new NumbersKeeper();
        String line = reader.readLine();
        while (!"exit".equalsIgnoreCase(line)) {
            switch (line) {
                case "avg":
                    System.out.println(numbersKeeper.getAvg()); break;
                case "max":
                    System.out.println(numbersKeeper.getMax()); break;
                case "min":
                    System.out.println(numbersKeeper.getMin()); break;
                case "count":
                    System.out.println(numbersKeeper.getCount()); break;
                case "all":
                    System.out.println(numbersKeeper.getFullResult()); break;
                default:
                    try {
                        long l = Long.parseLong(line);
                        numbersKeeper.addNumber(l);
                    } catch (NumberFormatException e) {
                        System.out.println(String.format("string %s in not number", line));
                    }
            }
            line = reader.readLine();
        }
    }
}
