package org.ortimaize;

public final class NumbersKeeper {
    private long max;
    private long min;
    private long count;
    private long sum;

    public NumbersKeeper() {
        max = Long.MIN_VALUE;
        min = Long.MAX_VALUE;
        count = 0;
        sum = 0;
    }

    public synchronized void addNumber(long number) {
        count++;
        if (count == 1) {
            sum = number;
            max = number;
            min = number;
        } else {
            sum += number;
            if (max < number) {
                max = number;
            } else if (min > number) {
                min = number;
            }

        }

    }

    public long getMax() {
        return max;
    }

    public long getMin() {
        return min;
    }

    public long getCount() {
        return count;
    }

    public synchronized double getAvg() {
        return getNonSyncAvg();
    }

    public synchronized NumbersResult getFullResult() {
        return new NumbersResult(max, min, getNonSyncAvg(), count);
    }

    private double getNonSyncAvg() {
        return sum/(count*1.0d);
    }
}
