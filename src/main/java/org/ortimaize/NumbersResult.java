package org.ortimaize;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class NumbersResult {
    private final long max;
    private final long min;
    private final double avg;
    private final long count;

    NumbersResult(long max, long min, double avg, long count) {
        this.max = max;
        this.min = min;
        this.avg = avg;
        this.count = count;
    }

    public long getMax() {
        return max;
    }

    public long getMin() {
        return min;
    }

    public double getAvg() {
        return avg;
    }

    public long getCount() {
        return count;
    }

    @Override
    public String toString() {
        NumberFormat formatter = new DecimalFormat("#0.00");
        return String.format("Result is max=%d, min=%d, count=%d, avg=%s (up to 2 digits)",
                max, min, count, formatter.format(avg));
    }
}
