package org.optimaize;

import org.junit.Test;
import org.ortimaize.NumbersKeeper;

import static org.junit.Assert.assertEquals;

public class NumberKeeperTest {
    @Test
    public void oneNumberTest() {
        NumbersKeeper numbersKeeper = new NumbersKeeper();
        numbersKeeper.addNumber(1);
        numbersKeeper.addNumber(1);
        double avg = numbersKeeper.getAvg();
        assertEquals(1.0, avg, 0.1);
        assertEquals(1, numbersKeeper.getMax());
        assertEquals(1, numbersKeeper.getMin());
        assertEquals(2, numbersKeeper.getCount());
    }

    @Test
    public void twoNumberTest() {
        NumbersKeeper numbersKeeper = new NumbersKeeper();
        numbersKeeper.addNumber(1);
        numbersKeeper.addNumber(2);
        double avg = numbersKeeper.getAvg();
        assertEquals(1.5, avg, 0.1);
        assertEquals(2, numbersKeeper.getMax());
        assertEquals(1, numbersKeeper.getMin());
        assertEquals(2, numbersKeeper.getCount());
    }

    @Test
    public void fourNumberTest() {
        NumbersKeeper numbersKeeper = new NumbersKeeper();
        numbersKeeper.addNumber(1);
        numbersKeeper.addNumber(2);
        numbersKeeper.addNumber(5);
        numbersKeeper.addNumber(7);
        double avg = numbersKeeper.getAvg();
        assertEquals(3.75, avg, 0.1);
        assertEquals(7, numbersKeeper.getMax());
        assertEquals(1, numbersKeeper.getMin());
        assertEquals(4, numbersKeeper.getCount());
    }
}
